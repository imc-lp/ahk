#SingleInstance Force

^SPACE::  Winset, Alwaysontop, , A
return

:*:!imc::Incident Management and Communications
return

:*:!gm::
Run, Chrome.exe "meet.new"
return

:*:!utcnow::
FormatTime, datetime, %A_NowUTC%, yyyy-MM-dd HH:mm 'UTC'
SetKeyDelay,0
Send, %datetime%
return

:*:!etnow::
cdt := A_Now
cdt += 3, hours
FormatTime, cdt,%cdt%, yyyy-MM-dd HH:mm 'ET'
SetKeyDelay,0
Send, %cdt%
return

:*:!now::
cdt := A_Now
cdt += 3, hours
FormatTime, datetime, %A_NowUTC%, yyyy-MM-dd HH:mm 'UTC'
FormatTime, cdt,%cdt%, yyyy-MM-dd HH:mm 'ET'
SetKeyDelay,0
Send, %datetime%{Space}|{Space}%cdt%
return

^!c::
HourOffset := 4
TimeZoneIdentifier := "EDT"
Send, ^c
FoundPos := RegExMatch(Clipboard, "[0-9]{2}:[0-9]{2} UTC", SubPat)
HourInteger := SubStr(SubPat,1,2)
MinuteInteger := SubStr(SubPat,4,2)

if (HourInteger > HourOffset){
	ReplacedStr := StrReplace(Clipboard, SubPat , SubPat " | " HourInteger - HourOffset ":" MinuteInteger " " TimeZoneIdentifier, OutputVarCount, -1)
	Clipboard := ReplacedStr
}else{
	if (HourInteger = HourOffset){
	ReplacedStr := StrReplace(Clipboard, SubPat , SubPat " | 00:" MinuteInteger " " TimeZoneIdentifier, OutputVarCount, -1)
	Clipboard := ReplacedStr
	}else{
	ReplacedStr := StrReplace(Clipboard, SubPat , SubPat " | " HourInteger + 24 - HourOffset ":" MinuteInteger " " TimeZoneIdentifier, OutputVarCount, -1)
	Clipboard := ReplacedStr
		}
	}
return